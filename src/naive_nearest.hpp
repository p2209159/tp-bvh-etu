#include "point.hpp"
#include "tree.hpp"

#include <vector>

int naive_nearest(const std::vector<Point>& points, const Point& query) ;
